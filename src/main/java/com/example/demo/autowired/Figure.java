package com.example.demo.autowired;

public interface Figure {

	double calculateArea();
}
