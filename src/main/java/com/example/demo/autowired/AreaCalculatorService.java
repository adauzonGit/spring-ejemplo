package com.example.demo.autowired;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AreaCalculatorService {

	@Autowired
	private List<Figure> figures;
	
	public double calculateAreas() {
		double area = 0;
		area = figures.stream().map(figure-> figure.calculateArea()).reduce(( x1, x2) -> x1 + x2).get();
		return area;
	}
}
