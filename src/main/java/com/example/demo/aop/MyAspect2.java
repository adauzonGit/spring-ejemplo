package com.example.demo.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(1)
public class MyAspect2 {
	private static final Logger log = LoggerFactory.getLogger(MyAspect2.class);

	@Before("PointcutExample.targetObjectMethod()")
	public void before(JoinPoint join) {
		log.info("Method name {}", join.getSignature().getName());
		log.info("Object type {}", join.getSignature().getDeclaringTypeName());
		log.info("Modifiers type {}", join.getSignature().getModifiers());
		log.info("Args {}", join.getArgs());

		log.info("before advice");
	}
}
