package com.example.demo.aop;

import org.aspectj.lang.annotation.Pointcut;

public class PointcutExample {

	
//	@Pointcut("execution(* com.example.demo.aop.TargetObject.*(..))")
//	@Pointcut("within(com.example.demo.aop.*)")
	@Pointcut("@annotation(DevAnotation)")
	public void targetObjectMethod() {
		
	}
}
