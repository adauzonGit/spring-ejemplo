package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cache.CacheProperties.EhCache;
import org.springframework.context.annotation.Bean;

import com.example.demo.aop.TargetObject;
import com.example.demo.atributo.Coche;
import com.example.demo.atributo.Motor;
import com.example.demo.autowired.AreaCalculatorService;
import com.example.demo.lifeCycle.ExplicitBean;
import com.example.demo.lifeCycle.LifeCycleBean;
import com.example.demo.profile.EnvironmentService;
import com.example.demo.scope.EjemploScope;

@SpringBootApplication
public class SpringCoreDependencyInyectionApplication {

	private static final Logger log = LoggerFactory.getLogger(SpringCoreDependencyInyectionApplication.class);

	@Bean
	public String getApplicationName() {
		return "pruebas";
	}
	
	@Bean(initMethod = "init", destroyMethod = "destroy")
	public ExplicitBean getBean() {
		return new ExplicitBean();
	}
	public static void main(String[] args) {
		var context = SpringApplication.run(SpringCoreDependencyInyectionApplication.class, args);
		var targetObject = context.getBean(TargetObject.class);
		targetObject.hello("Hello world with aop");
//		var ejemploScope = context.getBean(EjemploScope.class);
//		var ejemploScope2 = context.getBean(EjemploScope.class);
//
//		log.info("are beans equals : {} ", ejemploScope.equals(ejemploScope2));
//
//		var nombreAplicacion = context.getBean(String.class);
//		log.info("Este es el nombre de la aplicacion {} " , nombreAplicacion);
//		
//		
//		var calculator = context.getBean(AreaCalculatorService.class);
//		log.info("area de todas las figuras : {}", calculator.calculateAreas());
//		
//		
//		context.getBean(LifeCycleBean.class);
		
		

	}

}
