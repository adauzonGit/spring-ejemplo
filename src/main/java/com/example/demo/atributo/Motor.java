package com.example.demo.atributo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Motor {

	
	private String marca;
	
	private int modelo;

	public Motor () {
		
	}

	public String getMarca() {
		return marca;
	}

	public Motor( String marca,  int modelo) {
		super();
		this.marca = marca;
		this.modelo = modelo;
	}
	@Value("XL!")
	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getModelo() {
		return modelo;
	}
	@Value("2000")
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	@Override
	public String toString() {
		return "Motor [marca=" + marca + ", modelo=" + modelo + "]";
	}

}
